#include <thk_ledstrip_controller.h>


// Instanziiere Objekte für jeden LED-Streifen
#define LED_NUM 13
#define LED_PIN 33
int brightness = 100;

thk_LedStripController leds(LED_NUM,LED_PIN, brightness);     

byte red, green, blue;

#define POTI A0

#define NUM_COLORS 5
int color_number = 0;
int input = 0;

void setup()
{
    Serial.begin(115200);
    
    // Initialisierung der LED-Streifen
    leds.init();
    all_off();
    leds.set_brightness(255);
    leds.show();

    green = 100;
}

void loop()
{   
    if(Serial.available() > 0){
        Serial.print("Light show [0 - 10]: ");
        input = Serial.parseInt();
        Serial.print(input);
    }

    switch (input)
    {
        case 0:
            simple_light();  
            break;
        case 1:
            fade_in();
            delay(1000);
            fade_out();
            delay(1000);    
            break;
        case 2:
            leds.rainbow_effect(500);
            break;
        case 3:
            leds.running_light(10, 2, thk_LedStripController::Color(red,green,blue), 1);
            leds.running_light(10, 2, thk_LedStripController::Color(red,green,blue), 0);
            break;
        case 4:
            run_to_center(10,1);
            run_from_center(10,1);
            break;
        case 5:
            draw_comet(1, 50, 4);
            break;
        case 6:
            draw_marquee(1, 50);
            draw_marquee(0, 50);
            break;
        case 7:
            draw_twinkle_2();
            break;
        case 8:
            draw_twinkle_1();
            break;
    }
}

void simple_light(){
    leds.set_brightness(100);
    for(int k = 0; k < LED_NUM; k++){
        leds.set_pixel_color(k, thk_LedStripController::Color(red, green, blue));
    }
    leds.show(); 
}

void draw_comet(int variant, long speed, int comet_size){
    all_off();
    const uint8_t fade_amt = 51; // Fraction of 256 to fade a pixel by if it is chosen to be faded this pass
    const int delta_hue = 256; 

    static long hue = 0;
    static int direction  = 1;
    static int position = 0;

    static uint8_t red_fade = 0;
    static uint8_t green_fade = 0;
    static uint8_t blue_fade = 0;

    hue += delta_hue;
    position += direction;

    // Richtungswechsel, wenn der Komet eines der Enden erreicht hat
    if (position == (LED_NUM - comet_size) || position == 0){
        direction *= -1;
        color_number = random(NUM_COLORS);
    }
    
    if(variant == 1){
        // Anzeigen des Kometes an aktueller Position Variante 1
        for (int i = 0; i < comet_size; i++){
            int pixelHue = hue + (i * 65536L / LED_NUM);
            leds.set_pixel_color(position + i, leds.color_hsv(pixelHue));
        }
    }
    else {
        // Anzeigen des Kometes an aktueller Position Variante 2
        for (int i = 0; i < comet_size; i++){
            choose_color(color_number, 3);
            leds.set_pixel_color(position + i, thk_LedStripController::Color(red, green, blue));
        }
    }
    
    leds.show();
    delay(speed);
}

void run_to_center(int time, int length){
    for(int i = 0; i < int(LED_NUM/2); i++){
        leds.set_pixel_color(i, thk_LedStripController::Color(red, green, blue));
        leds.set_pixel_color(LED_NUM-i, thk_LedStripController::Color(red, green, blue));

        int last_pixel = i - length;

        leds.show();
        delay(time);

        if (last_pixel >= 0 && last_pixel <= int(LED_NUM/2))
        {
            leds.set_pixel_color(last_pixel, thk_LedStripController::Color(0, 0, 0));
            leds.set_pixel_color(LED_NUM-last_pixel, thk_LedStripController::Color(0, 0, 0));
        }

        leds.show();
        delay(time);
    }
}

void run_from_center(int time, int length){
    for(int i = 0; i < int(LED_NUM/2); i++){
        leds.set_pixel_color(int(LED_NUM/2)-1-i, thk_LedStripController::Color(red, green, blue));
        leds.set_pixel_color(int(LED_NUM/2)+i, thk_LedStripController::Color(red, green, blue));

        int last_pixel = i - length;

        leds.show();
        delay(time);

        if (last_pixel >= 0 && last_pixel <= int(LED_NUM/2)+1)
        {
            leds.set_pixel_color(int(LED_NUM/2)-1-last_pixel, thk_LedStripController::Color(0, 0, 0));
            leds.set_pixel_color(int(LED_NUM/2)+last_pixel, thk_LedStripController::Color(0, 0, 0));
        }

        leds.show();
        delay(time);
    }
}

void fade_in(){
    for (int i=20; i <= 100; i++){
            leds.set_brightness(i);
            for(int k = 0; k < LED_NUM; k++){
                leds.set_pixel_color(k, thk_LedStripController::Color(red, green, blue));
            }
            leds.show();
            delay(20);
    }
}

void fade_out(){
    for (int i=100; i >= 20; i--){
            leds.set_brightness(i);
            for(int k = 0; k < LED_NUM; k++){
                leds.set_pixel_color(k, thk_LedStripController::Color(red, green, blue));
            }
            leds.show();
            delay(20);
    }
}

void all_off(){
    leds.clear();
    leds.show();
}

void choose_color(int number, int intensity){
    byte a = map(intensity, 0, 10, 0, 255);
    switch (number)
    {
    case 0:
        // Rot
        red = a;
        green = 0;
        blue = 0;
        break;
    case 1:
        // Grün
        red = 0;
        green = a;
        blue = 0;
        break;
    case 2:
        // Blau
        red = 0;
        green = 0;
        blue = a;
        break;
    case 3:
        // Orange
        red = a;
        green = a;
        blue = 0;
        break;
    case 4:
        // Lila
        red = a;
        green = 0;
        blue = a;
        break;
    }

}

void draw_marquee(int direction, int speed){
    if (direction == 1){
        for(long firstPixelHue = 0; firstPixelHue < 65536; firstPixelHue += 256) 
        {
            for(uint16_t i = 0; i < LED_NUM; i++) 
            { 
                int pixelHue = firstPixelHue + (i * 65536L / LED_NUM);
                leds.set_pixel_color(i, leds.color_hsv(pixelHue));
            }

            static int scroll = 0;
            scroll++;

            for (int i=scroll % 6; i < LED_NUM - 1; i += 6){
                leds.set_pixel_color(i, thk_LedStripController::Color(0, 0, 0));
                leds.set_pixel_color(i+1, thk_LedStripController::Color(0, 0, 0));
            }

            leds.show();
            delay(speed);
        }    
    }
    else {
        for(long firstPixelHue = 65536; firstPixelHue >= 0 ; firstPixelHue -= 256) 
        {
            for(uint16_t i = 0; i < LED_NUM; i++) 
            { 
                int pixelHue = firstPixelHue + (i * 65536L / LED_NUM);
                leds.set_pixel_color(i, leds.color_hsv(pixelHue));
            }

            static int scroll = 0;
            scroll++;

            for (int i=LED_NUM - scroll % 6; i >= 0; i -= 6){
                leds.set_pixel_color(i, thk_LedStripController::Color(0, 0, 0));
                leds.set_pixel_color(i-1, thk_LedStripController::Color(0, 0, 0));
            }

            leds.show();
            delay(speed);
        } 
    }    
}

void draw_twinkle_2(){
    static int pass_count = 0;
    pass_count++;

    if (pass_count == LED_NUM / 4){
        pass_count = 0;
        all_off();
    }
    else {
        choose_color(random(NUM_COLORS), 5);
        leds.set_pixel_color(random(LED_NUM), thk_LedStripController::Color(red, green, blue));
    }
    
    leds.show();
    delay(200);
}

void draw_twinkle_1(){
    all_off();
    for (int i = 0; i < LED_NUM / 4; i++){
        choose_color(random(NUM_COLORS), 5);
        leds.set_pixel_color(random(LED_NUM), thk_LedStripController::Color(red, green, blue));
        leds.show();
        delay(200);
    }
}

/* *** * 
 * WIP *
 * *** */

void hue2rgb (long hue){
    hue = (hue * 1530L + 32768) / 65536;
    // Convert hue to R,G,B (nested ifs faster than divide+mod+switch):
    if (hue < 510) { // Red to Green-1
        blue = 0;
        if (hue < 255) { //   Red to Yellow-1
            red = 255;
            green = hue;       //     g = 0 to 254
        } else {         //   Yellow to Green-1
            red = 510 - hue; //     r = 255 to 1
            green = 255;
        }
    } else if (hue < 1020) { // Green to Blue-1
        red = 0;
        if (hue < 765) { //   Green to Cyan-1
            green = 255;
            blue = hue - 510;  //     b = 0 to 254
        } else {          //   Cyan to Blue-1
            green = 1020 - hue; //     g = 255 to 1
            blue = 255;
        }
    } else if (hue < 1530) { // Blue to Red-1
        green = 0;
        if (hue < 1275) { //   Blue to Magenta-1
            red = hue - 1020; //     r = 0 to 254
            blue = 255;
        } else { //   Magenta to Red-1
            red = 255;
            blue = 1530 - hue; //     b = 255 to 1
        }
    } else { // Last 0.5 Red (quicker than % operator)
        red = 255;
        green = blue = 0;
    }
}

void draw_comet_2(){
    const byte fade_amt = 128; // Fraction of 256 to fade a pixel by if it is chosen to be faded this pass
    const int comet_size = 6;   // Kometenlänge
    const int delta_hue = 128; 

    static long hue = 0;
    static int direction  = 1;
    static int position = 0;

    static byte red_fade = 0;
    static byte green_fade = 0;
    static byte blue_fade = 0;

    hue += delta_hue;
    position += direction;

    // Richtungswechsel, wenn der Komet eines der Enden erreicht hat
    if (position == (LED_NUM - comet_size) || position == 0){
        direction *= -1;
        color_number = random(NUM_COLORS);
    }
    
    // Anzeigen des Kometes an aktueller Position Variante 1
    for (int i = 0; i < comet_size; i++){
        int pixelHue = hue + (i * 65536L / LED_NUM);
        leds.set_pixel_color(position + i, leds.color_hsv(pixelHue));
    }

    if(direction == 1){
        for (int i = 0; i < position; i++){
            if (random(2) == 1){
                if ((leds.get_pixel_color(i) >> 16) > fade_amt){
                    red_fade = ((leds.get_pixel_color(i) >> 16) & 0xFF)-fade_amt;
                }
                else {
                    red_fade = 0;
                }
                
                if ((leds.get_pixel_color(i) >> 18) > fade_amt){
                    green_fade = ((leds.get_pixel_color(i) >> 8) & 0xFF)-fade_amt;
                }
                else {
                    red_fade = 0;
                }

                if ((leds.get_pixel_color(i)) > fade_amt){
                    blue_fade = ((leds.get_pixel_color(i)) & 0xFF)-fade_amt;
                }
                else {
                    red_fade = 0;
                }
                // red_fade = 0;
                // green_fade = 0;
                // blue_fade = 0;
                leds.set_pixel_color(i, thk_LedStripController::Color(red_fade,green_fade,blue_fade));
            } else {
                red_fade = 0;
                green_fade = 0;
                blue_fade = 0;
                leds.set_pixel_color(i, thk_LedStripController::Color(red_fade,green_fade,blue_fade));
            }
        }
    // } else {
    //     for (int i = LED_NUM; i > position; i--){
    //         if (random(2) == 1){
    //             if ((leds.get_pixel_color(i) >> 16) > fade_amt){
    //                 red_fade = ((leds.get_pixel_color(i) >> 16) & 0xFF)-fade_amt;
    //             }
    //             else {
    //                 red_fade = 0;
    //             }
                
    //             if ((leds.get_pixel_color(i) >> 18) > fade_amt){
    //                 green_fade = ((leds.get_pixel_color(i) >> 8) & 0xFF)-fade_amt;
    //             }
    //             else {
    //                 red_fade = 0;
    //             }

    //             if ((leds.get_pixel_color(i)) > fade_amt){
    //                 blue_fade = ((leds.get_pixel_color(i)) & 0xFF)-fade_amt;
    //             }
    //             else {
    //                 red_fade = 0;
    //             }
    //             // red_fade = 0;
    //             // green_fade = 0;
    //             // blue_fade = 0;
    //             leds.set_pixel_color(i, thk_LedStripController::Color(red_fade,green_fade,blue_fade));
    //         }
    //     }
    }
Serial.println(position);
    leds.show();
    delay(10);
}