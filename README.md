# **LED strip controller**

Eine Klasse für die Initializierung und Steuerung von LED-Streifen<br />
<br />


## **Voraussetzung:**
- [Adafruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel) (Getestet: Version 1.11.0)<br />
<br />


## **Installation:**
Um diese Klassen verwenden zu können, muss dieses Repository geklont und in das Libraries-Verzeichnis der Arduino-IDE kopiert werden.<br />
<br />


## **Anwendung:**
Zur Verwendung siehe zunächst das Beispiel `ledshow.ino`<br />
<br />

**Einbinden der Bibliothek:**
```Arduino
#include <thk_ledstrip_controller.h>
```

**Instanziieren:**
```Arduino
thk_LedStripController example_strip(LED_NUMBER, PIN, brightness);
```
- `LED_NUMBER`: Anzahl der LEDs am Streifen
- `PIN`: Pin für die Steuerung des LED-Streifens
- `brightness`: LED Helligkeit. Dieser Wert muss nicht mit übergeben werden, in dem Fall wird der vordefinierte wert von 64 genommen. Minimale Helligkeit (ausgeschaltet) = 0 und maximale Helligkeit = 255. <br />
<br />

### **Funktionen:**

**Ausschalten aller LEDs des Streifens:**
```Arduino
example_strip.clear();
``` 

<br />

**Farbeinstellung der LEDs:**
```Arduino
example_strip.set_pixel_color(thk_LedStripController::Color(red, green, blue));
``` 

```Arduino
example_strip.set_pixel_color(index, thk_LedStripController::Color(red, green, blue));
``` 

```Arduino
example_strip.set_pixel_color(start_index, amount, thk_LedStripController::Color(red, green, blue));
``` 

- `thk_LedStripController::Color(red, green, blue)`: Für red, green und blue Werte zwischen 0 und 255 angeben um die Helligkeit der LEDs einzustellen und somit eine Farbvariante zu generieren.
- `index`: Die LED Nummerierung welche gesteuert werden soll.
- `start_index`: Die LED Nummerierung von der begonnen werden soll.
- `amount`: Anzahl der LEDs, welche nach *start_index* angesteuert werden sollen.
<br />
<br />

**Ändern der Helligkeit:**
```Arduino
example_strip.set_brightness(brightness);
``` 
- `brightness`: Helligkeitswert von 0 bis 255 
<br />
<br />

**Blinken & Regenbogen Effekt:**
```C++
example_strip.blink(thk_LedStripController::Color(red, green, blue), wait_time);
``` 

```Arduino
example_strip.rainbow_effect(wait_time);
``` 

- `wait_time`: Die Geschwindigkeit, wie schnell geblinkt bzw der wechsel beim Regenbogen Effekt ausgeführt werden soll in Millisekunden.

*Hinweis:* Für die Dauer des Regenbogen Effekts, wird der Mikrocontroller blockiert und kann keine weiteren Befehle ausführen.
<br />
<br />

**Lichtlauf:**
```Arduino
example_strip.running_light(wait_time, length, thk_LedStripController::Color(red, green, blue), direction)
``` 

- `wait_time`: Die Zeit zwischen an/aus schalten und wechsel der LEDs
- `length`: Gibt die Anzahl, wie viele LEDs zusätzlich beim Lauf gleichzeitig an sein sollen. 
- `direction`: Gibt die Richtung des Laufes an. 1 = rechts und 0 = links